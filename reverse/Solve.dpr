program Solve;

{$APPTYPE CONSOLE}
{$O-}

uses
  SysUtils;

type
  PNode = ^TNode;
  TNode = record
    x, y, size : LongInt;
    l, r : PNode;
    reversed : Boolean;
  end;

var T : PNode;
    n, m, i, c : LongInt;
    s : string;

procedure swap(var a, b : PNode);
var c : PNode;
begin
  c := a;
  a := b;
  b := c;
end;

function Size(T : PNode) : LongInt;
begin
  if T = nil then
  begin
    Result := 0;
    Exit;
  end;
  Result := 1;
  if T^.l <> nil then
    Inc(Result, T^.l^.size);
  if T^.r <> nil then
    Inc(Result, T^.r^.size);
end;

function belletmen(var T : PNode) : Boolean;
begin
  Result := True;
  if T = nil then
    Exit;
  if T^.reversed then
  begin
    T^.reversed := False;
    swap(T^.l, T^.r);
    if T^.l <> nil then
      T^.l^.reversed := not T^.l^.reversed;
    if T^.r <> nil then
      T^.r^.reversed := not T^.r^.reversed;
  end;
end;

function merge(l, r : PNode) : PNode;
begin
  if l = nil then
  begin
    Result := r;
    belletmen(Result);
    Exit;
  end;
  if r = nil then
  begin
    Result := l;
    belletmen(Result);
    Exit;
  end;
  belletmen(l);
  belletmen(r);
  if l^.y < r^.y then
  begin
    l^.r := merge(l^.r, r);
    Result := l;
    Result^.size := Size(Result);
    Exit;
  end;
  r^.l := merge(l, r^.l);
  Result := r;
  Result^.size := Size(Result);
end;

procedure split(T : PNode; k : LongInt; var l, r : PNode);
begin
  if T = nil then
  begin
    l := nil;
    r := nil;
    Exit;
  end;
  belletmen(T);
  if k >= Size(T^.l) + 1 then
  begin
    split(T^.r, k - Size(T^.l) - 1, T^.r, r);
    l := T;
    if l <> nil then
      l^.size := Size(l);
    if r <> nil then
      r^.size := Size(r);
    belletmen(l);
    belletmen(r);
    Exit;
  end;
  split(T^.l, k, l, T^.l);
  r := T;
  if l <> nil then
    l^.size := Size(l);
  if r <> nil then
    r^.size := Size(r);
  belletmen(l);
  belletmen(r);
end;

function add(T : PNode; k, x : LongInt) : PNode;
var l, r, n : PNode;
begin
  New(n);
  n^.x := x;
  n^.y := Random(10000000);
  n^.l := nil;
  n^.r := nil;
  n^.size := 1;
  n^.reversed := False;
  split(T, k, l, r);
  l := merge(l, n);
  Result := merge(l, r);
end;

function reverse(T : PNode; left, right : LongInt) : PNode;
var l, m, r : PNode;
begin
  split(T, right, l, r);
  split(l, left - 1, l, m);
  m^.reversed := not m^.reversed;
  Result := merge(l, m);
  Result := merge(Result, r);
end;

function ask(T : PNode; k : LongInt) : LongInt;
var C : PNode;
begin
  C := T;
  while (belletmen(C)) and (C <> nil) and (Size(C^.l) + 1 <> k) do
  begin
    if k < Size(C^.l) + 1 then
      C := C^.l
    else
    begin
      k := k - Size(C^.l) - 1;
      C := C^.r;
    end;
  end;
  Result := C^.x;
end;

procedure parse(s : string);
var c, l, r : LongInt;
begin
  if s[1] = 'R' then
  begin
    Delete(s, 1, 8);
    c := Pos(' ', s);
    l := StrToInt(Copy(s, 1, c - 1)) ;
    Delete(s, 1, c);
    r := StrToInt(s);
    reverse(T, l, r);
  end
  else
  begin
    Delete(s, 1, 4);
    l := StrToInt(s);
    Writeln(ask(T, l));
  end;
end;

procedure print(T : PNode);
var i : LongInt;
begin
  for i := 1 to n do
    Write(ask(T, i), ' ');
end;

begin
  Reset(Input, 'reverse.in');
  Rewrite(Output, 'reverse.out');
  T := nil;
  Readln(n, m);
  for i := 1 to n do
  begin
    Read(c);
    T := add(T, i, c);
  end;
  Readln;
  for i := 1 to m do
  begin
    Readln(s);
    parse(s);
  end;
  print(T);
end.
