program Solve_New;

{$APPTYPE CONSOLE}

uses
  Math;

var parent,left,right,stack,x,y,num,num2:array[0..150005] of longint;
    i,n,last:longint;

procedure swapi(var a,b:longint);
var c:longint;
begin
  c:= a;
  a := b;
  b := c;
end;

procedure swap(i,j:longint);
begin
  swapi(x[i],x[j]);
  swapi(y[i],y[j]);
  num2[num[i]] := j;
  num2[num[j]] := i;
  swapi(num[i],num[j]);
end;

procedure sort(l,r: integer);
var
  i,j,m: integer;
begin
  i:=l; j:=r; m:=x[(l+r) DIV 2];
  repeat
    while x[i]<m do i:=i+1;
    while m<x[j] do j:=j-1;
    if i<=j then
    begin
      swap(i,j);
      i:=i+1; j:=j-1;
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;

begin
  reset(input,'tree.in');
  rewrite(output,'tree.out');
  fillchar(stack,sizeof(stack),0);
  fillchar(left,sizeof(left),0);
  fillchar(right,sizeof(right),0);
  fillchar(parent,sizeof(parent),0);
  readln(n);
  for i := 1 to n do
  begin
    readln(x[i],y[i]);
    num[i] := i;
    num2[i] := i;
  end;
  sort(1,n);
  inc(stack[0]);
  stack[1] := 1;
  for i := 2 to n do
  begin
    last := 0;
    while (stack[0] > 0) and (y[stack[stack[0]]] > y[i]) do
    begin
      last := stack[stack[0]];
      dec(stack[0]);
    end;
    left[i] := last;
    parent[last] := i;
    right[stack[stack[0]]] := i;
    parent[i] := stack[stack[0]];
    inc(stack[0]);
    stack[stack[0]] := i;
  end;
  writeln('YES');
  for i := 1 to n do
  begin
    //writeln(num2[parent[i]],' ',num2[left[i]],' ',num2[right[i]]);
    //num[parent[num2[i]]]
    writeln(num[parent[num2[i]]],' ',num[left[num2[i]]],' ',num[right[num2[i]]]);
  end;
end.
