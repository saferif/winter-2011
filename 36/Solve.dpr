program Solve;

{$APPTYPE CONSOLE}

{$O-,R+}

uses
  Math;

const
  maxN = 262143;

var b:array[1..800010] of int64;
    i,n,t,minf:longint;

procedure update(i,x:longint);
begin
  i := i + maxN;
  b[i] := x;
  while i > 1 do
  begin
    i := i div 2;
    b[i] := min(b[i * 2],b[i * 2 + 1]);
  end;
end;

function minR(l,r:longint):longint;
begin
  Result := MaxLongint;
  inc(l,maxN);
  inc(r,maxN);
  while l <= r do
  begin
    if l and 1 = 1 then
    begin
      Result := min(Result,b[l]);
      inc(l);
    end;
    if r and 1 = 0 then
    begin
      Result := min(Result,b[r]);
      dec(r);
    end;
    l := l div 2;
    r := r div 2;
  end;
end;

begin
  reset(input,'house36.in');
  rewrite(output,'house36.out');
  fillchar(b,sizeof(b),1);
  for i := 1 to 200000 do
    update(i,i);
  readln(n);
  for i := 1 to n do
  begin
    readln(t);
    if t > 0 then
    begin
      minf := minR(t,200000);
      update(minf,300000);
      writeln(minf);
    end
    else
    begin
      update(-t,-t);
    end;
  end;
end.
