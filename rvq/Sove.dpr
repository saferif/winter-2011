program ZC;

{$APPTYPE CONSOLE}

{$O-,R+}

uses
  Math;

const
  maxN = 262143;

var a,b:array[1..400010] of int64;
    i,r,x,y,m:longint;

procedure update(i,x:longint);
begin
  i := i + maxN;
  a[i] := x;
  b[i] := x;
  while i > 1 do
  begin
    i := i div 2;
    a[i] := max(a[i * 2],a[i * 2 + 1]);
    b[i] := min(b[i * 2],b[i * 2 + 1]);
  end;
end;

function maxR(l,r:longint):longint;
begin
  Result := -MaxLongint;
  inc(l,maxN);
  inc(r,maxN);
  while l <= r do
  begin
    if l and 1 = 1 then
    begin
      Result := max(Result,a[l]);
      inc(l);
    end;
    if r and 1 = 0 then
    begin
      Result := max(Result,a[r]);
      dec(r);
    end;
    l := l div 2;
    r := r div 2;
  end;
end;

function minR(l,r:longint):longint;
begin
  Result := MaxLongint;
  inc(l,maxN);
  inc(r,maxN);
  while l <= r do
  begin
    if l and 1 = 1 then
    begin
      Result := min(Result,b[l]);
      inc(l);
    end;
    if r and 1 = 0 then
    begin
      Result := min(Result,b[r]);
      dec(r);
    end;
    l := l div 2;
    r := r div 2;
  end;
end;

begin
  reset(input,'rvq.in');
  rewrite(output,'rvq.out');
  fillchar(b,sizeof(b),1);
  for i := 1 to 100000 do
    update(i,(Int64(i) * Int64(i)) mod 12345 + (Int64(i) * Int64(i) * Int64(i)) mod 23456);
  readln(r);
  for i := 1 to r do
  begin
    readln(x,y);
    if x < 0 then update(-x,y) else
    begin
      m := maxR(x,y);
      if i = r then
        write(m - minR(x,y))
      else
        writeln(m - minR(x,y));
    end;
  end;
end.
