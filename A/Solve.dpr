program Solve;

{$APPTYPE CONSOLE}
{$O-}

uses
  SysUtils;

type
  PNode = ^TNode;
  TNode = record
    x, y : LongInt;
    sum : Int64;
    l, r : PNode;
  end;

var T, LT, RT : PNode;
    n, i, l, r : LongInt;
    s : string;
    op, prev : Char;
    y : Int64;

procedure recalcsum(var T : PNode);
var ls, rs : Int64;
begin
  if T = nil then
    Exit;
  if T^.l = nil then
    ls := 0
  else
    ls := T^.l^.sum;
  if T^.r = nil then
    rs := 0
  else
    rs := T^.r^.sum;
  T^.sum := ls + rs + T^.x;
end;

function merge(l, r : PNode) : PNode;
begin
  if l = nil then
  begin
    Result := r;
    Exit;
  end;
  if r = nil then
  begin
    Result := l;
    Exit;
  end;
  if l^.y < r^.y then
  begin
    l^.r := merge(l^.r, r);
    recalcsum(l);
    Result := l;
    Exit;
  end;
  r^.l := merge(l, r^.l);
  recalcsum(r);
  Result := r;
end;

procedure split(T : PNode; k : LongInt; var l, r : PNode);
begin
  if T = nil then
  begin
    l := nil;
    r := nil;
    Exit;
  end;
  if k >= T^.x then
  begin
    split(T^.r, k, T^.r, r);
    l := T;
    recalcsum(l);
    recalcsum(r);
    Exit;
  end;
  split(T^.l, k, l, T^.l);
  r := T;
  recalcsum(l);
  recalcsum(r);
end;

function find(var T : PNode; k : LongInt) : Boolean;
var C : PNode;
begin
  C := T;
  while (C <> nil) and (C^.x <> k) do
    if k < C^.x then
      C := C^.l
    else
      C := C^.r;
  Result := not (C = nil);
end;

function add(T : PNode; k : LongInt) : PNode;
var l, r, n : PNode;
begin
  if find(T, k) then
  begin
    Result := T;
    Exit;
  end;
  New(n);
  n^.x := k;
  n^.sum := k;
  n^.y := Random(10000000);
  n^.l := nil;
  n^.r := nil;
  split(T, k, l, r);
  l := merge(l, n);
  Result := merge(l, r);
end;

function sumTree(T : PNode) : Int64;
begin
  if T = nil then
    Result := 0
  else
    Result := T^.sum;
end;

procedure parse(s : string; var op : Char; var l, r : LongInt);
var c : LongInt;
begin
  op := s[1];
  Delete(s, 1, 2);
  if op = '?' then
  begin
    c := Pos(' ', s);
    l := StrToInt(Copy(s, 1, c - 1));
    Delete(s, 1, c);
  end;
  r := StrToInt(s);
end;

begin
  Reset(Input, 'sum2.in');
  Rewrite(Output, 'sum2.out');
  T := nil;
  op := '+';
  y := 0;
  Readln(n);
  for i := 1 to n do
  begin
    Readln(s);
    prev := op;
    parse(s, op, l, r);
    if op = '+' then
    begin
      if prev = '?' then
        T := add(T, (Int64(r) + y) mod 1000000000)
      else
        T := add(T, r);
    end
    else
    begin
      split(T, l - 1, LT, T);
      split(T, r, T, RT);
      y := sumTree(T);
      Writeln(y);
      T := merge(LT, T);
      T := merge(T, RT);
    end;
  end;
end.
