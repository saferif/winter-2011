program Solve;

{$APPTYPE CONSOLE}
{$O-}

uses
  SysUtils;

type
  PNode = ^TNode;
  TNode = record
    x, y : LongInt;
    size : LongInt;
    l, r : PNode;
  end;

var T : PNode;
    n, m, i, c, newn : LongInt;
    s : string;

function Size(T : PNode) : LongInt;
begin
  if T = nil then
  begin
    Result := 0;
    Exit;
  end;
  Result := 1;
  if T^.l <> nil then
    Inc(Result, T^.l^.size);
  if T^.r <> nil then
    Inc(Result, T^.r^.size);
end;

function merge(l, r : PNode) : PNode;
begin
  if l = nil then
  begin
    Result := r;
    Exit;
  end;
  if r = nil then
  begin
    Result := l;
    Exit;
  end;
  if l^.y < r^.y then
  begin
    l^.r := merge(l^.r, r);
    Result := l;
    Result^.size := Size(Result);
    Exit;
  end;
  r^.l := merge(l, r^.l);
  Result := r;
  Result^.size := Size(Result);
end;

procedure split(T : PNode; k : LongInt; var l, r : PNode);
begin
  if T = nil then
  begin
    l := nil;
    r := nil;
    Exit;
  end;
  if k >= Size(T^.l) + 1 then
  begin
    split(T^.r, k - Size(T^.l) - 1, T^.r, r);
    l := T;
    if l <> nil then
      l^.size := Size(l);
    if r <> nil then
      r^.size := Size(r);
    Exit;
  end;
  split(T^.l, k, l, T^.l);
  r := T;
  if l <> nil then
    l^.size := Size(l);
  if r <> nil then
    r^.size := Size(r);
end;

function add(T : PNode; k, x : LongInt) : PNode;
var l, r, n : PNode;
begin
  New(n);
  n^.x := x;
  n^.y := Random(10000000);
  n^.l := nil;
  n^.r := nil;
  n^.size := 1;
  split(T, k, l, r);
  l := merge(l, n);
  Result := merge(l, r);
  Inc(newn);
end;

function del(T : PNode; k : LongInt) : PNode;
var l, m, r : PNode;
begin
  split(T, k, l, r);
  split(l, k - 1, l, m);
  Dispose(m);
  Result := merge(l ,r);
  Dec(newn);
end;

function Swap(T : PNode; i1, i2, len : LongInt) : PNode;
var _1, _2, _3, _4, _5 : PNode;
begin
  split(T, i2 + len, T, _5);
  split(T, i2 - 1, T, _4);
  split(T, i1 + len, T, _3);
  split(T, i1 - 1, _1, _2);
  Result := merge(_1, _4);
  Result := merge(Result, _3);
  Result := merge(Result, _2);
  Result := merge(Result, _5);
end;

function ask(T : PNode; k : LongInt) : LongInt;
var C : PNode;
begin
  C := T;
  while (C <> nil) and (Size(C^.l) + 1 <> k) do
  begin
    if k < Size(C^.l) + 1 then
      C := C^.l
    else
    begin
      k := k - Size(C^.l) - 1;
      C := C^.r;
    end;
  end;
  Result := C^.x;
end;

procedure parse(s : string);
var c, i, i1, len : LongInt;
begin
  case s[1] of
  'I':
  begin
    Delete(s, 1, 7);
    c := Pos(' ', s);
    i := StrToInt(Copy(s, 1, c - 1));
    Delete(s, 1, c);
    i1 := StrToInt(s);
    T := add(T, i, i1);
  end;
  'D':
  begin
    Delete(s, 1, 7);
    i := StrToInt(s);
    T := del(T, i);
  end;
  'S':
  begin
    Delete(s, 1, 5);
    c := Pos(' ', s);
    i := StrToInt(Copy(s, 1, c - 1));
    Delete(s, 1, c);
    c := Pos(' ', s);
    i1 := StrToInt(Copy(s, 1, c - 1));
    Delete(s, 1, c);
    len := StrToInt(s);
    T := Swap(T, i, i1, len);
  end;
  'A':
  begin
    Delete(s, 1, 4);
    i := StrToInt(s);
    Writeln(ask(T, i));
  end;
  end;
end;

procedure print(T : PNode);
var i : LongInt;
begin
  for i := 1 to newn do
    Write(ask(T, i), ' ');
end;

begin
  Reset(Input, 'array.in');
  Rewrite(Output, 'array.out');
  T := nil;
  Readln(n, m);
  newn := 0;
  for i := 1 to n do
  begin
    Read(c);
    T := add(T, i, c);
  end;
  Readln;
  for i := 1 to m do
  begin
    Readln(s);
    parse(s);
  end;
  print(T);
end.
